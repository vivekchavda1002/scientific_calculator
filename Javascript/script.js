/*append value in Result Box*/
function number(val) {
    document.getElementById('result').value += val;
}
/*evaluate result*/
function equal() {
    try{
        setResult(eval(getResult()))
    }catch(err){
        setResult("Error")
    }
}
/*Clear screen*/
function erase(val) {
    if (val.match('clear')) {
        let result = getResult();
        result = result.substring(0, result.length - 1)
        setResult(result);
    } else if (val.match('allClr')) {
        setResult("");
    }
}
/* Getting the result from input field*/
function getResult() {
    let result = document.getElementById('result').value;
    return result;
}
/* setting the result from input field*/
function setResult(val) {
    document.getElementById('result').value = val
}
function log(val) {
    if (val.match('log')) {
        setResult(Math.log10(getResult()));
    } else if (val.match('in')) {
        setResult(Math.log(getResult()));
    }
}
function squareRoot() {
    let result = getResult();
    setResult(Math.pow(result, 2))
}
function power() {
    let result = getResult();
    setResult(Math.pow(result, 2));
}
function pie() {
    let result = getResult();;
    setResult(eval(result * 3.14159265359));
}
var factorial = n => (n <= 0) ? 1 : n * factorial(n - 1);
function fact() {
    let result = getResult();
    setResult(factorial(result));
}
function reciprocal() {
    let result = getResult();
    setResult(eval(`1/${result}`));
}
function twoRootX() {
    let result = getResult();
    setResult(eval(`2*Math.sqrt(${result})`));
}
function tenX() {
    let result = getResult();
    setResult(eval(`10 ** ${result}`));
}
function negativeNumber(val) {
    let result = getResult();
    if (val.match('mod')) {
        if (!result > 0) {
            setResult(eval(`${result} * (-1)`))
        }
    } else if (val.match('neg')) {
        setResult(eval(`${result} * (-1)`))
    }
}
/* All trigo functions*/
function trigonometry(val) {
    let result = getResult();
    if (val.match('sin')) {
        setResult(Math.sin(result))
    } else if (val.match('cos')) {
        setResult(Math.cos(result))
    } else if (val.match('tan')) {
        setResult(Math.tan(result))
    }
}
/* All Aggregate Functions */
function aggregate(val) {
    let result = getResult();
    if (val.match('acos')) {
        setResult(Math.acos(result))
    } else if (val.match('abs')) {
        setResult(Math.abs(result))
    } else if (val.match('acosh')) {
        setResult(Math.acosh(result))
    } else if (val.match('arg')) {
        setResult(Math.abs(result))
    } else if (val.match('asin')) {
        setResult(Math.asin(result))
    }
}
/* Memory Store oprations closure*/
function memory() {
    let m = 0;
    return {
        mPlus: () => m = eval(`${m} + ${getResult()}`),
        mMinus: () => m = eval(m - getResult()),
        mStore: () => m = getResult(),
        mClear: () => m = 0,
        mRecall: () => setResult(m)
    }
}
let memoryOpration = memory();
/* function for Scientific Exponent */
function fe() {
    let result = getResult();
    setResult(Number.parseFloat(result).toExponential(2))
}
/* function for Euler e Opration */
function expo() {
    let result = getResult();
    setResult(Math.exp(result))
}